package step_definitions;

import Tools.SelenideTools;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.open;

public class MainPageStepsDef {
    private static final By CREDIT_BUTTON = By.xpath("(//*[@class='sc-jJEKmz ejANpH'])[3]");

    SelenideTools selTools = new SelenideTools();

        @И("^открываем страницу$")
        public void goToHome() {
            open("https://www.mtsbank.ru/");
        }

        @И("^переходим в раздел 'Кредиты'$")
        public void goToCredit() {
            selTools.clickButton(CREDIT_BUTTON);
        }
}

